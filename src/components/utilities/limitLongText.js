const textReducer = (text, limit) => {
  const reducedText = [];
  text.reduce((accumulator, current) => {
    if (accumulator + current.length <= limit) {
      reducedText.push(current);
    }
    return accumulator + current.length;
  }, 0);
  return reducedText;
};

export const limitLongText = (text, limit = 17) => {
  if (text.length > limit) {
    let limitedText = [];

    limitedText = textReducer(text.split(" "), limit);

    if (limitedText.length < 2) {
      const limitedTextByChar = textReducer(text.split(""), limit);
      return `${limitedTextByChar.join("")} ...`;
    }

    return `${limitedText.join(" ")} ...`;
  }
  return text;
};
