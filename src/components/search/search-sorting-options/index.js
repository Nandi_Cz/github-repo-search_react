import React from "react";
import { useSelector } from "react-redux";
import { connect } from "react-redux";
import { search, setSortingOptionsData } from "../../../reducers/actions";
import classes from "./search-sorting-options.module.css";

const SearchSortingOptions = (props) => {
  const currentState = useSelector((state) => state);

  const handleSortingOptionsChange = (event) => {
    if (event.target.name === "searchSortType") {
      props.setSortingOptionsData(event.target.value, currentState.orderType);
    } else {
      props.setSortingOptionsData(currentState.sortType, event.target.value);
    }
    props.search();
  };
  let isVisible = false;
  if (currentState.searchResults.length) {
    isVisible = true;
  } else {
    isVisible = false;
  }
  return (
    <div
      className={`${classes.sortingOptions} ${
        isVisible ? classes.visible : classes.hidden
      }`}
    >
      <div className={classes.sort}>
        <p>Sort by:</p>
        <input
          type="radio"
          id="sort-by-default"
          value="default"
          name="searchSortType"
          onChange={handleSortingOptionsChange}
          defaultChecked
        />
        <label htmlFor="sort-by-default">default</label>

        <input
          type="radio"
          id="sort-by-stars"
          value="stars"
          name="searchSortType"
          onChange={handleSortingOptionsChange}
        />
        <label htmlFor="sort-by-stars">stars</label>

        <input
          type="radio"
          id="sort-by-forks"
          value="forks"
          name="searchSortType"
          onChange={handleSortingOptionsChange}
        />
        <label htmlFor="sort-by-forks">forks</label>
      </div>
      <div className={classes.order}>
        <p>Order by:</p>
        <input
          type="radio"
          id="order-by-desc"
          value="desc"
          name="searchOrderType"
          onChange={handleSortingOptionsChange}
          defaultChecked
        />
        <label htmlFor="order-by-desc">desc</label>

        <input
          type="radio"
          id="order-by-asc"
          value="asc"
          name="searchOrderType"
          onChange={handleSortingOptionsChange}
        />
        <label htmlFor="order-by-asc">asc</label>
      </div>
      <div className={classes.totalResults}>
        <p>Total results: </p>
        {currentState.searchResultsTotalCount}
      </div>
    </div>
  );
};

const mapDispatchToProps = {
  search,
  setSortingOptionsData,
};

export default connect(null, mapDispatchToProps)(SearchSortingOptions);
