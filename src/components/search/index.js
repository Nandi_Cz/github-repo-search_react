import React, { useState } from "react";
import searchIcon from "../../images/search-icon.svg";
import { connect } from "react-redux";
import Menu from "../menu";
import { search, setFormData, searchReset } from "../../reducers/actions";
import SearchResultList from "./search-result-list";
import SearchSortingOptions from "../search/search-sorting-options";
import classes from "./search.module.css";
import LoadingIndicator from "../ui/loading-indicator";
import { useSelector } from "react-redux";

const Search = (props) => {
  const isLoading = useSelector((state) => state.isLoading);
  const [searchText, setSearchText] = useState("");
  const [validationText, setValidationText] = useState("");

  const handleSearchFormSubmit = (event) => {
    event.preventDefault();
    if (isValidSearchText()) {
      const searchType = document.forms.searchForm.elements.searchType.value;
      props.setFormData(encodeURIComponent(searchText), searchType);
      props.search();
    }
  };

  const handleFormReset = () => {
    setSearchText("");
    setValidationText("");
    document.getElementById("search-form").reset();
    props.searchReset();
  };

  const onSearchTextChangeHandler = (event) => {
    setSearchText(event.target.value);
  };

  const isValidSearchText = () => {
    if (!searchText || searchText.length < 3) {
      setValidationText("Text too short");
      return false;
    } else {
      setValidationText("");
      return true;
    }
  };

  return (
    <div>
      <Menu />
      <form
        id="search-form"
        name="searchForm"
        onSubmit={handleSearchFormSubmit}
        className={classes.form}
      >
        <fieldset className={classes.basicFieldset}>
          <div className={classes.searchText}>
            <div className={classes.validationWarning}>{validationText}</div>
            <div className={classes.searchTextInput}>
              <label htmlFor="search-text">Search by:*</label>
              <input
                type="text"
                id="search-text"
                onChange={onSearchTextChangeHandler}
              />
            </div>
          </div>
          <div className={classes.searchType}>
            <p>In:</p>
            <input
              type="radio"
              id="search-type-name"
              value="name"
              name="searchType"
              defaultChecked
            />
            <label htmlFor="search-type-name">name</label>

            <input
              type="radio"
              id="search-type-description"
              value="description"
              name="searchType"
            />
            <label htmlFor="search-type-description">description</label>

            <input
              type="radio"
              id="search-type-readme"
              value="readme"
              name="searchType"
            />
            <label htmlFor="search-type-readme">readme</label>
          </div>
        </fieldset>
        <div className={classes.searchButtons}>
          <button type="submit">
            <img
              src={searchIcon}
              alt="search icon"
              className={classes.searchIcon}
            />
          </button>
          <button type="button" onClick={handleFormReset}>
            Reset
          </button>
        </div>
      </form>
      {isLoading && <LoadingIndicator />}
      <SearchSortingOptions />
      <SearchResultList />
    </div>
  );
};

const mapDispatchToProps = {
  search,
  setFormData,
  searchReset,
};

export default connect(null, mapDispatchToProps)(Search);
