import React from "react";
import { useSelector } from "react-redux";
import { limitLongText } from "../../utilities/limitLongText";
import classes from "./search-result-list.module.css";

const SearchResultList = () => {
  const listData = useSelector((state) => state.searchResults);
  let list = [];

  if (listData.length) {
    list = listData.map((listItem) => {
      return (
        <li key={listItem.id} className={classes.listItem}>
          <div className={classes.listItemNameSection}>
            <div className={classes.listItemName}>
              {listItem.name && limitLongText(listItem.name, 40)}
            </div>
            <div className={classes.listItemFullName}>
              {listItem.full_name && listItem.full_name}
            </div>
          </div>
          <div className={classes.listItemStarSection}>
            <div>
              Stars: {listItem.stargazers_count && listItem.stargazers_count}
            </div>
            <div>
              Watchers: {listItem.watchers_count && listItem.watchers_count}
            </div>
          </div>
          <div className={classes.listItemForkSection}>
            <div>Forks: {listItem.forks_count && listItem.forks_count}</div>
            <div>
              Issues: {listItem.open_issues_count && listItem.open_issues_count}
            </div>
          </div>
          <div className={classes.listItemDescriptionSection}>
            {listItem.description && limitLongText(listItem.description, 80)}
          </div>
          <div className={classes.listItemLanguageSection}>
            <div className={classes.listItemLanguage}>
              {listItem.language && listItem.language}
            </div>
          </div>
          <div className={classes.listItemDateSection}>
            <div>
              Created at:{" "}
              {listItem.created_at &&
                listItem.created_at.slice(0, 10).replaceAll("-", ".")}
            </div>
            <div>
              Updated at:{" "}
              {listItem.updated_at &&
                listItem.updated_at.slice(0, 10).replaceAll("-", ".")}
            </div>
          </div>
          <div className={classes.listItemOwnerSection}>
            By: {listItem.owner.login && listItem.owner.login}
          </div>
          <div className={classes.listItemAvatarSection}>
            {listItem.owner.avatar_url && (
              <img
                src={listItem.owner.avatar_url}
                alt="Avatar"
                className={classes.listItemImage}
              ></img>
            )}
          </div>
        </li>
      );
    });
  }

  const isList = list.length ? true : false;
  return (
    <div className={classes.searchResultListContainer}>
      <ul className={classes.searchResultList}>{isList && list}</ul>
    </div>
  );
};

export default SearchResultList;
