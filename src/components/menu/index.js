import React from "react";
import { NavLink } from "react-router-dom";
import classes from "./menu.module.css";

const Menu = () => {
  const showMenu = (event) => {
    event.currentTarget.querySelector("span").classList.toggle(classes.show);
  };
  const hideMenu = () => {
    document
      .getElementsByClassName(classes.menuItemsContainer)[0]
      .classList.toggle(classes.show);
  };
  return (
    <header className={classes.appHeader}>
      <div className={classes.logoContainer}>
        <h1>Repo Search App</h1>
      </div>
      <ul className={classes.headerMenu}>
        <li>
          <NavLink to="/search" exact>
            Search
          </NavLink>
        </li>
        <li>
          <NavLink to="/history" exact>
            History
          </NavLink>
        </li>
      </ul>
      <div className={classes.headerHamburgerMenu} onMouseLeave={hideMenu}>
        <div className={classes.hamburgerMenu} onMouseEnter={showMenu}>
          <div></div>
          <div></div>
          <div></div>
          <span className={classes.menuItemsContainer}>
            <p>Page 1</p>
            <p>Page 2</p>
          </span>
        </div>
      </div>
    </header>
  );
};

export default Menu;
