import React from "react";
import classes from "./loading-indicator.module.css";
export default function LoadingIndicator() {
  return (
    <div className={classes.ldsRollerContainer}>
      <div className={classes.ldsRoller}>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  );
}
