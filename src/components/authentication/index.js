import React from "react";
import { connect } from "react-redux";
import { authenticate } from "../../reducers/actions";
import classes from "./authentication.module.css";
import LoadingIndicator from "../ui/loading-indicator";
import { useSelector } from "react-redux";

const Authentication = (props) => {
  const isLoading = useSelector((state) => state.isLoading);
  return (
    <div className={classes.loginContainer}>
      <h1>Welcome, please authenticate with github!</h1>
      {isLoading && <LoadingIndicator />}
      <button onClick={() => props.authenticate()}>Authenticate</button>
    </div>
  );
};

const mapDispatchToProps = {
  authenticate,
};

export default connect(null, mapDispatchToProps)(Authentication);
