import {
  githubAuthentication,
  githubGetSearchRequest,
} from "../containers/pizzly";
import axios from "axios";

axios.defaults.baseURL = "https://api.github.com";

export const authenticate = () => async (dispatch) => {
  dispatch({ type: "AUTHENTICATE" });

  try {
    const { authId } = await githubAuthentication();
    console.log(
      "[Action middleware thunk: authenticate success] authId:",
      authId
    ); /////
    dispatch({ type: "AUTHENTICATE_SUCCESS", authId: authId });
  } catch (error) {
    console.log("[Action middleware thunk: authenticate fail] error:", error); /////
    dispatch({ type: "AUTHENTICATE_FAIL" });
  }
};

export const setFormData = (searchText, searchType) => {
  console.log("[Action creator: SET_FORM_DATA] ", searchText, searchType); /////
  return {
    type: "SET_FORM_DATA",
    searchText,
    searchType,
  };
};

export const setSortingOptionsData = (sortType, orderType) => {
  console.log("[Action creator: SET_SORTING_DATA] ", sortType, orderType); /////
  return {
    type: "SET_SORTING_DATA",
    sortType,
    orderType,
  };
};

export const search = () => async (dispatch, getState) => {
  dispatch({ type: "SEARCH" });

  const state = getState();

  const searchParams = `q=${state.searchText}+in:${state.searchType}+sort:${state.sortType}-${state.orderType}`;
  console.log("[Action creator: SEARCH]", searchParams); /////
  try {
    const results = await githubGetSearchRequest(state.authId, searchParams);
    const resultsData = await results.json();
    console.log(
      "[Action middleware thunk: search success] results:",
      resultsData
    ); /////
    dispatch({ type: "SEARCH_SUCCESS", searchResults: resultsData });
  } catch (error) {
    console.log("[Action middleware thunk: search fail] error:", error); /////
    dispatch({ type: "SEARCH_FAIL" });
  }
};

export const searchReset = () => {
  console.log("[Action creator: SEARCH_RESET] "); /////
  return {
    type: "SEARCH_RESET",
  };
};
