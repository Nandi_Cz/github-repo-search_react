import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import { githubAuthentication } from "../../containers/pizzly";
import * as actions from "../actions";

jest.mock("../../containers/pizzly");

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

afterEach(() => {
  githubAuthentication.mockRestore();
});

test("Dispatch success path actions", () => {
  const expectedAuthId = "asdf";

  githubAuthentication.mockImplementation(() =>
    Promise.resolve({ authId: expectedAuthId })
  );

  const expectedActions = [
    { type: "AUTHENTICATE" },
    { type: "AUTHENTICATE_SUCCESS", authId: expectedAuthId },
  ];

  const store = mockStore({});

  return store.dispatch(actions.authenticate()).then(() => {
    expect(store.getActions()).toEqual(expectedActions);
  });
});
