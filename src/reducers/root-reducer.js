const initialState = {
  authId: null,
  isAuthenticated: false,
  isAuthenticationError: false,
  searchText: "",
  searchType: "",
  sortType: "default",
  orderType: "desc",
  searchResults: [],
  searchResultsTotalCount: 0,
  isSearchError: false,
  isLoading: false,
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case "AUTHENTICATE":
      console.log("[rootReducer action: AUTHENTICATE]"); /////
      return {
        ...state,
        authId: null,
        isAuthenticated: false,
        authentiactionError: false,
        isLoading: true,
      };
    case "AUTHENTICATE_SUCCESS":
      console.log("[rootReducer action: AUTHENTICATE_SUCCESS]"); /////
      return {
        ...state,
        authId: action.authId,
        isAuthenticated: true,
        isLoading: false,
      };
    case "AUTHENTICATE_FAIL":
      console.log("[rootReducer action: AUTHENTICATE_FAIL]"); /////
      return {
        ...state,
        isAuthenticationError: true,
        isLoading: false,
      };
    case "SET_FORM_DATA":
      console.log("[rootReducer action: SET_FORM_DATA]"); /////
      return {
        ...state,
        searchText: action.searchText,
        searchType: action.searchType,
      };
    case "SET_SORTING_DATA":
      console.log("[rootReducer action: SET_SORTING_DATA]"); /////
      return {
        ...state,
        sortType: action.sortType,
        orderType: action.orderType,
      };
    case "SEARCH":
      console.log("[rootReducer action: SEARCH]"); /////
      return {
        ...state,
        searchResults: [],
        searchResultsTotalCount: 0,
        isSearchError: false,
        isLoading: true,
      };
    case "SEARCH_SUCCESS":
      console.log("[rootReducer action: SEARCH_SUCCESS]"); /////
      return {
        ...state,
        searchResults: action.searchResults.items,
        searchResultsTotalCount: action.searchResults.total_count,
        isLoading: false,
      };
    case "SEARCH_FAIL":
      console.log("[rootReducer action: SEARCH_FAIL]"); /////
      return {
        ...state,
        isSearchError: true,
        isLoading: false,
      };
    case "SEARCH_RESET":
      console.log("[rootReducer action: SEARCH_RESET]"); /////
      return {
        ...state,
        searchText: "",
        searchType: "",
        sortType: "default",
        orderType: "desc",
        searchResults: [],
        searchResultsTotalCount: 0,
        isSearchError: false,
      };
    default:
      return state;
  }
};

export default rootReducer;
