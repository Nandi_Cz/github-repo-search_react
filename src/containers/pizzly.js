import Pizzly from "pizzly-js";

const pizzly = new Pizzly({
  host: "https://git-nandor-repository-search.herokuapp.com",
});

const github = pizzly.integration("github", {
  setupId: "d3ecc7a3-1b42-49ed-ac14-f42ccc89e4a5",
});

export const githubAuthentication = () => {
  return github.connect();
};

export const githubGetSearchRequest = (authId, params) => {
  return github.auth(authId).get(`/search/repositories?${params}`);
};
