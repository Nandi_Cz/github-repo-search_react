import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import "./App.css";
import Search from "./components/search";
import { connect } from "react-redux";
import Authentication from "./components/authentication";
import SearchHistory from "./components/search-history";

function App(props) {
  return (
    <div className="App">
      <Router>
        {!props.isAuthenticated && <Authentication />}
        {props.isAuthenticated && <Redirect to={"/search"} />}
        <Switch>
          <Route path="/search" exact component={Search} />
          <Route path="/history" exact component={SearchHistory} />
        </Switch>
      </Router>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.isAuthenticated,
    state: state,
  };
};

export default connect(mapStateToProps)(App);
