const path = require("path");

module.exports = {
  rootDir: path.join(__dirname, "."),
  moduleDirectories: ["node_modules", path.join(__dirname, "./src")],
  moduleNameMapper: {
    ".module.css$": "identity-obj-proxy",
    ".css$": require.resolve("./test/style-mock.js"),
    ".svg$": require.resolve("./test/style-mock.js"),
  },
  transform: {
    "^.+.js?$": "babel-jest",
  },
  transformIgnorePatterns: ["node_modules/(?!@pizzly-js)/"],
  moduleFileExtensions: ["js"],
};
